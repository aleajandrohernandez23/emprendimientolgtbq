import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Cliente } from "./cliente.entity";
import { Producto_Agregado } from "./producto_agregado.entity";


@Entity()
export class Compra{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fecha: Date;

    @Column()
    finalizada: boolean;

    @ManyToOne(() => Cliente, (cliente) => cliente.compra, {nullable: false })
    cliente: Cliente;
    
    @OneToMany(() => Producto_Agregado, (producto_agregado) => producto_agregado.compra)
    producto_agregado: Producto_Agregado;
}