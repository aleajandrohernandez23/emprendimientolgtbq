import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Cliente } from "./cliente.entity";
import { Producto } from "./producto.entity";

@Entity()
export class Resena{
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    mensaje: string;

    @Column()
    calificacion: number;
    
    @ManyToOne(() => Producto, (producto) => producto.resena, {nullable: false })
    producto: Producto;
    
    @ManyToOne(() => Cliente, (cliente) => cliente.resena, {nullable: false })
    cliente: Cliente;

    
}