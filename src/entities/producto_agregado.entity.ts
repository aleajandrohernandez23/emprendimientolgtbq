import { Column, Entity, ManyToOne,PrimaryGeneratedColumn } from "typeorm";
import { Producto } from "./producto.entity";
import { Compra } from "./compra.entity";

@Entity()
export class Producto_Agregado{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cantidad: number;

    @ManyToOne(() => Compra, (compra) => compra.producto_agregado, {nullable: false })
    compra: Compra; 
    
    @ManyToOne(() => Producto, (producto) =>producto.producto_agregado, {nullable: false })
    producto: Producto;
    
}
