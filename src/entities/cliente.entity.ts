import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Compra } from './compra.entity';
import { Favorito } from "./favorito.entity";
import { Resena } from "./resena.entity";

@Entity()
export class Cliente{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    apellido: string;

    @Column()
    telefono: string;

    @Column()
    email: string;

    @OneToMany(() =>Compra, (compra) => compra.cliente)
    compra: Compra[];

    @OneToMany(() => Favorito, (favorito) => favorito.cliente)
    favorito: Favorito;

    @OneToMany(() => Resena, (resena) => resena.cliente)
    resena: Resena;


}
