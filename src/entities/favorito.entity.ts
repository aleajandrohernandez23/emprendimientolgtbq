
import { Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Producto } from "./producto.entity";
import { Cliente } from "./cliente.entity";


@Entity()
export class Favorito{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Producto, (producto) => producto.favorito, {nullable: false })
    producto: Producto;

    @ManyToOne(() => Cliente, (cliente) => cliente.favorito, {nullable: false }) 
    cliente: Cliente;
}