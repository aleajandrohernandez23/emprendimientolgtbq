import { Column, Entity,ManyToOne,OneToMany,PrimaryGeneratedColumn } from "typeorm";
import { Compra } from "./compra.entity";
import { Vendedor } from "./vendedor.entity";
import { Favorito } from "./favorito.entity";
import { Resena } from "./resena.entity";
import { Producto_Agregado } from "./producto_agregado.entity";
import { Categorias } from "src/common/enums";

@Entity()
export class Producto{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;
    
    @Column()
    valor: number;

    @Column()
    categoria: Categorias;

    @ManyToOne(() => Vendedor, (vendedor) => vendedor.producto, {nullable: false })
    vendedor: Vendedor;

    @OneToMany(() => Favorito, (favorito) => favorito.producto)
    favorito: Favorito;

    @OneToMany(() => Resena, (resena) => resena.producto)
    resena: Resena[];

    @OneToMany(() => Producto_Agregado, (producto_agregado) => producto_agregado.producto)
    producto_agregado: Producto_Agregado[]; 
}