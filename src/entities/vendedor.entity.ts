import { Categorias } from 'src/common/enums';
import { Producto } from './producto.entity';
import { Column, Entity, OneToMany,PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Vendedor{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nit: number;

    @Column()
    nombre: string;

    @Column()
    email: string;                                  

    @Column()
    categoria: Categorias;

    @OneToMany(() => Producto, (producto) => producto.vendedor)
    producto: Producto;


}