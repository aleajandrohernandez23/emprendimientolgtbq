import { Categorias } from "src/common/enums";

export class ProductoDTO {
    id: number;
    nombre: string;
    valor: number;
    categoria: Categorias;
    vendedorId: number;
}