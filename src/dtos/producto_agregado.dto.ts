export class Producto_AgregadoDTO {
    id: number;
    cantidad: number;
    compraId?: number;
    productoId?: number;
}