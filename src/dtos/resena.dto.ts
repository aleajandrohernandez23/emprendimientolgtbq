export class ResenaDTO {
    id: number;
    mensaje: string;
    calificacion: number;
    clienteId: number;
    productoId: number;
}