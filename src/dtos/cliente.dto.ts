export class ClienteDTO{
    id: number;
    nombre: string;
    apellido: string;
    celular: string;
    email: string;
}