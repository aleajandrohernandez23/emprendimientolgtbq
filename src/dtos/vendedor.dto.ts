import { Categorias } from "src/common/enums";

export class VendedorDTO {
    id: number;
    nit: number;
    nombre: string;
    email: string;
    categoria: Categorias;
}