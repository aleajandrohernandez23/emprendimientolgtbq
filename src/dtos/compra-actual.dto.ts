import { Producto_AgregadoDTO } from './producto_agregado.dto';


export class CompraActualDTO{
    totalItems: number;
    totalCompra: number;
    producto_Agregado: Producto_AgregadoDTO[];

}