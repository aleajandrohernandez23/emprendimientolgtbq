import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompraActualDTO } from 'src/dtos/compra-actual.dto';
import { CompraDTO } from 'src/dtos/compra.dto';
import { Compra } from 'src/entities/compra.entity';
import { Producto_Agregado } from 'src/entities/producto_agregado.entity';
import { Repository } from 'typeorm';




@Injectable()
export class CompraService {
    constructor(
        @InjectRepository(Compra)
        private compraRepository: Repository<Compra>,
        @InjectRepository(Producto_Agregado)
        private productoAgregadoRepository: Repository<Producto_Agregado>
    ) { }

    async verCompraActual(clienteId): Promise<CompraActualDTO> {
        const compraActual = await this.compraRepository.findOne({
            where: {
                cliente: { id: clienteId },
                fecha: null
            }
        })
        const producto_Agregado = await this.productoAgregadoRepository.find(
            {
                where: {
                    compra: compraActual
                },
                relations: ['producto']
            }
        );

        let totalCompra = 0;
        totalCompra = producto_Agregado.map(p=>p.producto.valor * p.cantidad).reduce((total, p) => p + total)

        return {
            producto_Agregado,
            totalCompra,
            totalItems: producto_Agregado.length
        }


    }



    crearCompra(compra: CompraDTO): Promise<Compra> {
        return this.compraRepository.save({
            ...compra,
            cliente: { id: compra.clienteId }
        });
    }

    listarCompra() {
        return this.compraRepository.find();

    }

    eliminarCompra(id: number) {
        return this.compraRepository.delete(id)
    }

    actualizarCompra(id: number, compra: CompraDTO) {
        return this.compraRepository.update(id, compra);
    }

}