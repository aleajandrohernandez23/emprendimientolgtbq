import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Producto_Agregado } from '../entities/producto_agregado.entity';
import { Producto_AgregadoDTO } from 'src/dtos/producto_agregado.dto';


@Injectable()
export class Producto_AgregadoService{
    constructor(
        @InjectRepository(Producto_Agregado)
        private producto_agregadoRepository: Repository<Producto_Agregado>
    ){   
    }

      crearProducto_Agregado(producto_agregado:Producto_AgregadoDTO):Promise<Producto_Agregado> {
        
        return this.producto_agregadoRepository.save({
            ...producto_agregado,
            producto: {id: producto_agregado.productoId},
            compra: {id: producto_agregado.compraId}
        }); 
    }

    listarProducto_Agregado() { 
        return this.producto_agregadoRepository.find();

    }

    eliminarProducto_Agregado(id: number) {
        return this.producto_agregadoRepository.delete(id)
    }

    actualizarProducto_Agregado(id: number, producto_agregado: Producto_AgregadoDTO) {
        return this.producto_agregadoRepository.update(id, producto_agregado);
    }

}