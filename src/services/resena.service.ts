import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Resena } from '../entities/resena.entity';
import { ResenaDTO } from 'src/dtos/resena.dto';


@Injectable()
export class ResenaService{
    constructor(
        @InjectRepository(Resena)
        private resenaRepository: Repository<Resena>
    ){   
    }

      crearResena(resena:ResenaDTO):Promise<Resena> {
        return this.resenaRepository.save({
            ...resena,
            cliente: { id: resena.clienteId },
            producto: { id: resena.productoId }
        });    
    }

    listarResena() { 
        return this.resenaRepository.find();

    }

    eliminarResena(id: number) {
        return this.resenaRepository.delete(id)
    }

    actualizarResena(id: number, resena: ResenaDTO) {
        return this.resenaRepository.update(id, resena);
    }

}