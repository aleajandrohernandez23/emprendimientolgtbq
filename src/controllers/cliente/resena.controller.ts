import { ResenaService } from '../../services/resena.service';
import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import {Request, Response} from 'express';


@Controller('resena')
export class ResenaController {
    
    constructor(private ResenaService: ResenaService){

    }

    @Post()
    crearResena(@Res() response: Response, @Req() request: Request){
        console.log(request.body); 
        this.ResenaService.crearResena(request.body).then(res =>{
            console.log(res);
            response.status(HttpStatus.CREATED).json(res);
            
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })

    }

    @Get()
    listarResena(@Res() response: Response, @Req() request: Request){
        this.ResenaService.listarResena().then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
       
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Delete('/:id')
    eliminarResena(@Res() response: Response, @Param('id') id: number){
        
        this.ResenaService.eliminarResena(id).then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Put('/:id')
    actualizarResena(@Res() response: Response, @Req() request: Request, @Param('id') id: number){
        this.ResenaService.actualizarResena(id, request.body).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }) .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

}
