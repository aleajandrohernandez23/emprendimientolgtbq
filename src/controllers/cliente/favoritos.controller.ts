import { FavoritoService } from '../../services/favorito.service';
import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import {Request, Response} from 'express';


@Controller('favorito')
export class FavoritoController {
    
    constructor(private FavoritoService: FavoritoService){

    }

    @Post()
    crearFavorito(@Res() response: Response, @Req() request: Request){
        console.log(request.body); 
        this.FavoritoService.crearFavorito(request.body).then(res =>{
            console.log(res);
            response.status(HttpStatus.CREATED).json(res);
            
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })

    }

    @Get()
    listarFavoritos(@Res() response: Response, @Req() request: Request){
        this.FavoritoService.listarFavorito().then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
       
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Delete('/:id')
    eliminarFavorito(@Res() response: Response, @Param('id') id: number){
        
        this.FavoritoService.eliminarFavorito(id).then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Put('/:id')
    actualizarFavorito(@Res() response: Response, @Req() request: Request, @Param('id') id: number){
        this.FavoritoService.actualizarFavorito(id, request.body).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }) .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

}
