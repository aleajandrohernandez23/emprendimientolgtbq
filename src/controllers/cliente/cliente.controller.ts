import { ClienteService } from './../../services/cliente.service';
import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import {Request, Response} from 'express';


@Controller('cliente')
export class ClienteController {
    
    constructor(private ClienteService: ClienteService){

    }

    @Post()
    crearCliente(@Res() response: Response, @Req() request: Request){
        console.log(request.body); 
        this.ClienteService.crearCliente(request.body).then(res =>{
            console.log(res);
            response.status(HttpStatus.CREATED).json(res);
            
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })

    }

    @Get()
    listarClientes(@Res() response: Response, @Req() request: Request){
        this.ClienteService.listarCliente().then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
       
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Delete('/:id')
    eliminarCliente(@Res() response: Response, @Param('id') id: number){
        
        this.ClienteService.eliminarCliente(id).then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Put('/:id')
    actualizarCliente(@Res() response: Response, @Req() request: Request, @Param('id') id: number){
        this.ClienteService.actualizarCliente(id, request.body).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }) .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

}
