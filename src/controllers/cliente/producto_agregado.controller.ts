import { Producto_AgregadoService } from '../../services/producto_agregado.service';
import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import {Request, Response} from 'express';


@Controller('producto_agregado')
export class Producto_AgregadoController {
    
    constructor(private Producto_AgregadoService: Producto_AgregadoService){

    }

    @Post()
    crearProducto_Agregado(@Res() response: Response, @Req() request: Request){
        console.log(request.body); 
        this.Producto_AgregadoService.crearProducto_Agregado(request.body).then(res =>{
            console.log(res);
            response.status(HttpStatus.CREATED).json(res);
            
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })

    }

    @Get()
    listarProducto_Agregados(@Res() response: Response, @Req() request: Request){
        this.Producto_AgregadoService.listarProducto_Agregado().then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
       
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Delete('/:id')
    eliminarProducto_Agregado(@Res() response: Response, @Param('id') id: number){
        
        this.Producto_AgregadoService.eliminarProducto_Agregado(id).then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Put('/:id')
    actualizarProducto_Agregado(@Res() response: Response, @Req() request: Request, @Param('id') id: number){
        this.Producto_AgregadoService.actualizarProducto_Agregado(id, request.body).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }) .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

}
