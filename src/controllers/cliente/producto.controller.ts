import { ProductoService } from '../../services/producto.service';
import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import {Request, Response} from 'express';


@Controller('producto')
export class ProductoController {
    
    constructor(private ProductoService: ProductoService){

    }

    @Post()
    crearProducto(@Res() response: Response, @Req() request: Request){
        console.log(request.body); 
        this.ProductoService.crearProducto(request.body).then(res =>{
            console.log(res);
            response.status(HttpStatus.CREATED).json(res);
            
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })

    }

    @Get()
    listarProducto(@Res() response: Response, @Req() request: Request){
        this.ProductoService.listarProducto().then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
       
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Delete('/:id')
    eliminarProducto(@Res() response: Response, @Param('id') id: number){
        
        this.ProductoService.eliminarProducto(id).then(res =>{
            console.log(res);
            response.status(HttpStatus.OK).json(res);
        })  .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

    @Put('/:id')
    actualizarProducto(@Res() response: Response, @Req() request: Request, @Param('id') id: number){
        this.ProductoService.actualizarProducto(id, request.body).then(res =>{
            response.status(HttpStatus.OK).json(res);
        }) .catch(err => {
            console.log(err);
            response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        })
    }

}
