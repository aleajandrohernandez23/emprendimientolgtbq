import { Producto_AgregadoController } from './controllers/cliente/producto_agregado.controller';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cliente} from './entities/cliente.entity';
import { Compra } from './entities/compra.entity';
import { Producto } from './entities/producto.entity';
import { ClienteService } from './services/cliente.service';
import { Producto_Agregado } from './entities/producto_agregado.entity';
import { Resena } from './entities/resena.entity';
import { Vendedor } from './entities/vendedor.entity';
import { Favorito } from './entities/favorito.entity';
import { ClienteController } from './controllers/cliente/cliente.controller';
import { CompraController } from './controllers/cliente/compra.controller';
import { CompraService } from './services/compra.service';
import { Producto_AgregadoService } from './services/producto_agregado.service';
import { VendedorController } from './controllers/cliente/vendedor.controller';
import { VendedorService } from './services/vendedor.service';
import { ProductoService } from './services/producto.service';
import { ProductoController } from './controllers/cliente/producto.controller';
import { ResenaController } from './controllers/cliente/resena.controller';
import { ResenaService } from './services/resena.service';
import { FavoritoService } from './services/favorito.service';
import { FavoritoController } from './controllers/cliente/favoritos.controller';


const entities = [Cliente, Compra, Producto,Producto_Agregado,Resena,Vendedor,Favorito]; 
@Module({
  imports: [ 
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'adminL',
      password: '1234',
      database: 'lucommer',
      entities: entities,
      synchronize: true,
    }),
  TypeOrmModule.forFeature(entities)],
  controllers: [AppController,ClienteController,CompraController,Producto_AgregadoController,ProductoController,VendedorController,ResenaController,FavoritoController],
  
  providers: [AppService,ClienteService,CompraService,Producto_AgregadoService,ProductoService,VendedorService,ResenaService,FavoritoService],
  

})
export class AppModule {}
